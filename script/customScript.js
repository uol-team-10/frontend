function loadJSON(callback) {

    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open('GET', './JSON/dashboard.json', true);
    xobj.onreadystatechange = function() {
        if (xobj.readyState == 4 && xobj.status == "200") {

            callback(xobj.responseText);
        }
    };
    xobj.send(null);
}

var deviceID = 'emulatorxxxxxxxxxxxxxxxxxxxxxxxx';

function getDateTime(timestamp) {
    https: //stackoverflow.com/questions/847185/convert-a-unix-timestamp-to-time-in-javascript
        var date = new Date(timestamp * 1000);
    var year = date.getFullYear();
    var month = date.getMonth();
    var day = date.getDate();
    var hours = date.getHours();
    var minutes = "0" + date.getMinutes();
    var seconds = "0" + date.getSeconds();
    return day + '/' + month + '/' + year + ' ' + hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
}

function getDeviceName() {
    const Http = new XMLHttpRequest();
    const url = 'http://localhost:5000/devices/' + deviceID + '/name';
    Http.open("GET", url, true);
    Http.send();

    Http.onreadystatechange = (e) => {
        if (Http.readyState == 4 && Http.status == 200) {
            data = JSON.parse(Http.responseText);
            document.getElementById("device").textContent = data.name;
        }
    }
}

function pollData() {
    // https://www.w3schools.com/js/js_json_http.asp
    const Http = new XMLHttpRequest();
    const url = 'http://localhost:5000/devices/' + deviceID + '/telemetry/latest';
    Http.open("GET", url, true);
    Http.send();

    Http.onreadystatechange = (e) => {
        if (Http.readyState == 4 && Http.status == 200) {
            data = JSON.parse(Http.responseText);

            // console.log(data);
            temp = data.data.temperature;
            humidity = data.data.humidity;
            lux = data.data.lux;
            moist = data.data.moisture;
            timestamp = getDateTime(data.ts);
            console.log(data);
        }
    }

    // set the url to the nw type of telemetry you want ( copy paste [LINE 46-48])

    /*const url = 'http://localhost:5000/devices/' + deviceID + '/telemetry/WHAT-YOU-NEED';
    Http.open("GET", url, true);
    Http.send();

        - after that, copy paste [LINE 50-61], BUT with the var names you want THAT MATCH THE ONES AT ../dashboard.html [LINE 270] -

    Http.onreadystatechange = (e) => {
            if (Http.readyState == 4 && Http.status == 200) {
                data = JSON.parse(Http.responseText);

                // console.log(data);
                NEWTEMP = data.data.temperature;   <----- this should match variable names at ../dashboard.html [LINE 270]
                NEWHUM = data.data.humidity;
                NEWLUX = data.data.lux;
                ETC
                console.log(data);
            }

    */
}